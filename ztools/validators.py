import phonenumbers
from django.utils import timezone
from django.core.exceptions import ValidationError

def non_init_zero(value):
    if not value.isdigit():
        raise ValidationError(f'Debe ser numérico')
    if value.startswith('0'):
        raise ValidationError('Cedula No puede empezar con 0')

def year_validation(value):
    current_year = timezone.now().year
    if value > current_year:
        raise ValidationError("El año no puede ser mayor al año actual.")
    if value < 1900:  # Puedes ajustar este límite según tus necesidades
        raise ValidationError("El año debe ser igual o mayor a 1900.")


def validate_phone_number(value):
    """ Phone Numbers Validators, this needs to install phonenumbers 
    """
    try:
        phone_number = phonenumbers.parse(value)
        if not phonenumbers.is_valid_number(phone_number):
            raise ValidationError('El número de teléfono no es válido.')
    except phonenumbers.phonenumberutil.NumberParseException:
        raise ValidationError('El formato del número de teléfono no es válido.')