from rest_framework.renderers import JSONRenderer
from rest_framework.views import exception_handler


class CustomJSONRenderer(JSONRenderer):

    def render(self, data, accepted_media_type=None, renderer_context=None):
        if data != None:
            if 'error' in data and data['error']:
                data = {
                    'error': True,
                    'message': data.pop('message') if 'message' in data else 'No message.',
                    'data': data['data'] if 'data' in data else data
                }

            else:
                data = {
                    'error': False,
                    'message': data.pop('message') if 'message' in data else 'No message.',
                    'data': data['data'] if 'data' in data else data
                }

            return super().render(data, accepted_media_type, renderer_context)

        return super().render({
            'status': 'NO_CONTENT',
            'message': 'No response',
            'data': [
                'No response'
            ]
        }, accepted_media_type, renderer_context)


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)
    print('exc', exc)
    # Now add the HTTP status code to the response.
    if response is not None:
        try:
            message = exc.detail.lower().capitalize()
        except:
            try:
                message = list(exc.detail.values())[0].title()
            except:
                message = ''
                try:
                    for a in list(exc.detail.values()):
                        message += a[0]+' '
                except:
                    try:
                        message += exc.detail[0]
                    except:
                        message = 'An error occurred'
                        # message += response.data['detail']
        
        response_data = {
            'error': True,
            'message': message,
            'data': [
                response.data
            ]
        }
        response.data = response_data
    return response
