TOOLS_STATUS = [
    ('En Manteniento','En Manteniento'),
    ('Disponible','Disponible'),
    ('En Uso','En Uso'),
]

VEHICLE_TYPES = [
    ('normal','normal'),
    ('batea','batea'),
    ('lowBoy','lowBoy'),
    ('tolva','tolva'),
]

FILTER_TYPES = [
    ('Aceite','Aceite'),
    ('Gasoil','Gasoil'),
    ('N/A','N/A'),
]

VEHICLE_STATUS_CHOICES = [
    ('disponible', 'disponible'),
    ('enUso', 'enUso'),
    ('enMantenimiento', 'enMantenimiento'),
    ('fueraDeServicio', 'fueraDeServicio'),
    ('reservado', 'reservado'),
]

MAINTENANCE_TYPE_CHOICES = [
    ('Preventivo', 'Preventivo'),
    ('Correctivo', 'Correctivo'),
    ('Inspeccion', 'Inspección'),
    ('Rotacion_neumaticos', 'Rotación de Neumáticos'),
    ('Cambio_aceite', 'Cambio de Aceite'),
]