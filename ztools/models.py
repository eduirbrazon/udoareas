import uuid
from django.db import models
from .validators import non_init_zero, validate_phone_number
class AbstractUUIDPK(models.Model):
    
    class Meta:
        abstract=True

    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class AbstractPersonModel(AbstractUUIDPK):
   
    class Meta:
        abstract=True
    
    first_name = models.CharField(max_length=50)
    second_names = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=32)
    second_last_names = models.CharField(max_length=64, null=True)
    phone_number = models.CharField(max_length=24, validators=[validate_phone_number,])
    dni = models.CharField(max_length=10, validators=[non_init_zero], unique=True, verbose_name='Numero de Cedula')
    
    def full_name(self):
        return f'{self.first_name} {self.last_name}'
