from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from udoProject.views import APKView
from jwtSettings.views import GetAccesToken, TokenVerifyView, LoggoutView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/login', GetAccesToken.as_view(), name='token_obtain_pair'),
    path('api/logout', LoggoutView.as_view(), name='logout'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/auth/verify', TokenVerifyView.as_view(), name='token_verify'),
    path('api/vehicles', include('apps.vehicles.urls')),
    path('api/persons', include('apps.persons.urls')),
    path('api/users', include('apps.users.urls')),
    path('apk', APKView.as_view(), name='specific-file'),
]
