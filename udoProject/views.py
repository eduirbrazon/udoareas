# views.py
import os
from django.http import HttpResponse, FileResponse, Http404
from django.conf import settings
from rest_framework.views import APIView


def serve_specific_file(request):
    file_path = os.path.join(settings.STATICFILES_DIRS, '/main.go')
    if os.path.exists(file_path):
        return FileResponse(open(file_path, 'rb'))
    else:
        # Manejar el caso en que el archivo no exista
        pass

class APKView(APIView):
    def get(self, request):
        if settings.STATICFILES_DIRS:
                    file_path = os.path.join(settings.STATICFILES_DIRS[0], 'tsc.apk')
                    try:
                        print(' here ', file_path)
                        return FileResponse(open(file_path, 'rb'), as_attachment=True, filename='tsc.apk')
                    except IOError:
                        # Manejar el error si el archivo no existe
                        raise Http404('El archivo main.go no se encontró')
        else:
            raise Http404('Directorio de archivos estáticos no configurado correctamente')
