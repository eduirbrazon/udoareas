from rest_framework.permissions import BasePermission, IsAdminUser

class AdministratorPermission(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.admin)

class AdministratorPermission(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.coordinator)

class AdministratorPermission(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.boss)
