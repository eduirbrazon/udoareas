import uuid
from django.db import models
from ..persons.models import EmployeeModel
from django.contrib.auth.models import AbstractBaseUser,BaseUserManager, PermissionsMixin

class UserManagerModel(BaseUserManager):
    def create_user(self, password, **extraField):
        user = self.model( **extraField)
        user.set_password(password)
        user.save()
        return user
    def set_password(self,password):
        self.model.set_password(self,raw_password=password)
        self.model.save()
        return self

    def create_superuser(self, **extraField):
        user = self.create_user(**extraField)
        user.is_staff = True
        user.save()
        return user

class UserModel(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False, primary_key=True)
    username = models.CharField('Nombre de usuario', unique=True, max_length=100)
    password = models.CharField(max_length=128)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    admin = models.BooleanField(default=False)
    coordinator = models.BooleanField(default=False)
    boss = models.BooleanField(default=False)

    person = models.OneToOneField(EmployeeModel, on_delete=models.CASCADE, null=True, related_name='user')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    session_token = models.TextField(null=True, default=None)

    objects=UserManagerModel()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['password',]

    def rol(self):
        if self.is_staff:
            return 'superSu'
        if self.admin:
            return 'admin'
        if self.coordinator:
            return 'coordinador'
        if self.boss:
            return 'presidente'
    
    class Meta:
        db_table = 'users'
