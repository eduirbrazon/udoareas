from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser
from .models import UserModel
from .serializers import UserSerializer, UsersReadSerializer


class UsersApiView(APIView):
    permission_classes = [IsAdminUser,]
    def get(self, request):
        users = UserModel.objects.filter(is_active=True)
        serializer_user = UsersReadSerializer(users, many=True)
        return Response(serializer_user.data)
    
    def post(self, request):
        rol = request.data['rol']
        data ={
            **request.data
        }
        rol_map = {
            'superSu': 'is_staff',
            'admin': 'admin',
            'coordinador':'coordinator',
            'presidente':'boss'
        }
        data[rol_map[rol]]=True
        serializer = UserSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data)

    def delete(self, request, pk):
        user = UserModel.objects.get(id=pk)
        if user == request.user:
            return Response({'message':'No puede eliminarse a si mismo'}, status=400)
        user.is_active = False
        user.save()
        return Response({'message':'Usuario Eliminado con Exito'})
