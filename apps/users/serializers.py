from rest_framework import serializers
from .models import UserModel

class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=30, write_only=True)
    
    class Meta:
        model = UserModel
        fields = '__all__'
        
    
    def update(self, instance, validated_data):
        user = super().update(instance, validated_data)
        if 'password' in validated_data:
            user.set_password(validated_data['password'])
            user.save()
        return user

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user

class UsersReadSerializer(serializers.ModelSerializer):
    person_n = serializers.CharField(source='person.full_name', allow_null=True)
    class Meta:
        model = UserModel
        fields = ['id','username','rol', 'is_active', 'person_n']
    