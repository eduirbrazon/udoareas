from django.test import TestCase

# 
class TestUsersAPI(TestCase):
 
    def test_users_endpoint(self):
        resp = self.client.post('http://localhost:8741/api/login', data={'username': 'admin', 'password': 'admin'})
        self.assertEqual(resp.status_code, 200)
        token = resp.json()['data']['access']
        response = self.client.get('http://localhost:8741/api/users', HTTP_AUTHORIZATION=f'Bearer {token}')
        self.assertEqual(response.status_code, 200)
 
    def test_users_post(self):
        response = self.client.post('http://localhost:8741/api/users', data={
    "username": "user",
    "password": "123456",
    "email": "  [email protected]",
    "first_name": "user",
    "last_name": "user",
    "rol": "superSu"
})
        self.assertEqual(response.status_code, 201)
        
