from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import UsersApiView

router = DefaultRouter(trailing_slash=False)
# router.register(r'/?', )

urlpatterns = [
    path('', UsersApiView.as_view()),
    path('/<pk>', UsersApiView.as_view()),
]
urlpatterns+=router.urls
