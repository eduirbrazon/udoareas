from django.db import models
from ztools.models import AbstractPersonModel

class EmployeeModel(AbstractPersonModel):
    
    code_number = models.CharField(max_length=50, unique=True, verbose_name='código')

    class Meta:
        db_table = 'employee'
        verbose_name = "Empleado"

class ClientModel(AbstractPersonModel):
    
    class Meta:
        db_table = 'clients'    
