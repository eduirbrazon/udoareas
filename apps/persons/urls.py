from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import EmployeeViewset

router = DefaultRouter(trailing_slash=False)
router.register(r'/employees/?', EmployeeViewset)

urlpatterns = [
    
]
urlpatterns+=router.urls
