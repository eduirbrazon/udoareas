from rest_framework import serializers
from .models import EmployeeModel, ClientModel

class EmployeeSerializers(serializers.ModelSerializer):
    full_name = serializers.CharField(read_only=True)
    class Meta:
        model = EmployeeModel
        fields = '__all__'

class ClientSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ClientModel
        fields = '__all__'

class ShortEmployee(serializers.ModelSerializer):
    full_name = serializers.CharField(read_only=True)
    class Meta:
        model = EmployeeModel
        fields = ['id', 'full_name', 'code_number']

