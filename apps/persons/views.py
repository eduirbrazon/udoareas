from rest_framework import status
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from .models import EmployeeModel
from .serializers import EmployeeSerializers, ShortEmployee

class EmployeeViewset(ModelViewSet):
    queryset = EmployeeModel.objects.all()
    serializer_class = EmployeeSerializers
    permission_classes= [IsAuthenticated,]

    
    @action(detail=False, methods=['get'], permission_classes=[IsAuthenticated,] )
    def short(self, request, pk=None):
        serializer = ShortEmployee(self.queryset.filter(user=None), many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

