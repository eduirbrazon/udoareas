from django.db import models
from ztools.models import AbstractUUIDPK
from ztools.constants import TOOLS_STATUS
from ..persons.models import EmployeeModel


class ToolsModels(AbstractUUIDPK):
    """Herramientas"""
    name = models.CharField(max_length=50)
    trademark = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    bought_date = models.DateField()
    status = models.CharField(choises=TOOLS_STATUS, max_length=14)

    class Meta:
        db_table = 'tools'

class ConsumableModels(AbstractUUIDPK):
    """Consumibles de las herramientas"""

    name = models.CharField(max_length=50, unique=True)
    quantity = models.IntegerField()
    used_for = models.ForeignKey(ToolsModels, on_delete=models.CASCADE, verbose_name="what tool uses it")

    class Meta:
        db_table = 'consumables'

class WithdrawToolsModel(AbstractUUIDPK):
    """Control de Salida de Herramientas"""
    class Meta:
        db_table = 'withdraw_toolscard'
    
    employee = models.ForeignKey(EmployeeModel, verbose_name="", on_delete=models.CASCADE, related_name="taken_tools")
    return_date = models.DateTimeField(null=True)

class WithdrawItemsModel(AbstractUUIDPK):
    """Detalles u objetos de retiro de herramientas"""
    tool = models.ForeignKey(ToolsModels, on_delete=models.DO_NOTHING)
    w_tool = models.ForeignKey(WithdrawToolsModel, on_delete=models.CASCADE)

    class Meta:
        db_table = 'withdraw_items'

class WithodrawToolItem(AbstractUUIDPK):
    consumable = models.ForeignKey(ConsumableModels, verbose_name=(""), on_delete=models.DO_NOTHING)
    consumable_quantity = models.IntegerField()
    

    w_tool = models.ForeignKey(WithdrawToolsModel, on_delete=models.CASCADE)
    class Meta:
        db_table = 'withdraw_itemsTools'
