from rest_framework import serializers
from .models import (ToolsModels, ConsumableModels, WithdrawToolsModel, WithdrawItemsModel)

class ToolsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ToolsModels
        fields = '__all__'
        
class ConsumableSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ConsumableModels
        fields = '__all__'

class WithdrawItemsSerializer(serializers.ModelSerializer):

    class Meta:
        model = WithdrawItemsModel
        fields = '__all__'


class WithdrawToolsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = WithdrawToolsModel
        fields = '__all__'


