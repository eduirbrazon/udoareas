from django.db import models
from ztools.models import AbstractUUIDPK
from ztools.validators import year_validation
from ztools.constants import VEHICLE_TYPES, FILTER_TYPES, VEHICLE_STATUS_CHOICES, MAINTENANCE_TYPE_CHOICES
from  apps.persons.models import EmployeeModel, ClientModel


class HydrocarbonModel(models.Model):
    """Aceite y Gasolina"""

    name = models.CharField(max_length=50, unique=True)
    model = models.CharField(max_length=50, unique=True)
    code = models.CharField(max_length=50, unique=True)
    quantity = models.FloatField(default=0)
    class Meta:
        db_table = 'hidrocarburos'

class SparePartsModel(AbstractUUIDPK):
    """ Repuestos """
    class Meta:
        db_table = 'spare parts'

    name = models.CharField(max_length=50)
    model = models.CharField(max_length=50)
    item_code = models.CharField(max_length=50, unique=True)
    quantity = models.IntegerField()
    filter_type = models.CharField(choices=FILTER_TYPES, max_length=50, default='N/A')
    supplier = models.CharField(max_length=255, blank=True, null=True)
    min_stock_level = models.IntegerField(default=0)
    purchase_date = models.DateField(null=True, blank=True)

class VehicleModel(AbstractUUIDPK):
    """Maquinas y Vehiculos"""
    class Meta:
        db_table = 'vehicles'
        ordering = ['number']

    vehicle_type = models.CharField(choices=VEHICLE_TYPES, max_length=50, default='Normal')
    number = models.IntegerField(unique=True)
    trademark = models.CharField(max_length=50)
    vehicle_model = models.CharField(max_length=50)
    license_plate = models.CharField(max_length=7, unique=True)
    engine_serial = models.CharField(max_length=10, unique=True, null=True, blank=True)
    bodywork_serial = models.CharField(max_length=18, unique=True)
    year = models.IntegerField(validators=[year_validation])
    color = models.CharField(max_length=50)
    insurance_policy = models.CharField(max_length=14, unique=True, null=True)
    insurance_policy_expiration_date = models.DateField(null=True)
    status = models.CharField(max_length=50, choices=VEHICLE_STATUS_CHOICES, default='available')
    current_location = models.CharField(max_length=255, blank=True, null=True)
    
    current_fuel = models.IntegerField(default=0, help_text="Combustible actual en litros")
    max_fuel_capacity = models.IntegerField(help_text="Capacidad máxima de combustible en litros")
    current_oil = models.IntegerField(default=0, help_text="Acite actual en litros")
    max_oil_capacity = models.IntegerField(help_text="Capacidad máxima de Aceite en litros")
    
    mileage = models.BigIntegerField()
    kms_to_maintenance = models.IntegerField()
    worked_hours = models.IntegerField()
    
    oil_filter = models.ForeignKey(SparePartsModel, on_delete=models.CASCADE, 
                                   verbose_name="filtro de aceite", related_name='oil_filter_cars', null=True)
    primary_diesel_filter = models.ForeignKey(SparePartsModel, verbose_name="Filtro de Gasoil primario",
                                              on_delete=models.CASCADE, related_name='primary_diesel', null=True)                         
    secondary_diesel_filter = models.ForeignKey(SparePartsModel, verbose_name="Filtro de Gasoil secundario", 
                                                on_delete=models.CASCADE, related_name='secondary_diesel', null=True)


class MaintenanceSheetModel(AbstractUUIDPK):
    """ Ficha de Mantenimiento """
    class Meta:
        db_table = 'maintenances'
    
    vehicle = models.ForeignKey(VehicleModel, on_delete=models.DO_NOTHING, related_name="maintenances")
    operator = models.ForeignKey(EmployeeModel, on_delete=models.DO_NOTHING)
    kms_at_maintenance = models.IntegerField()
    kms_to_next_maintenance = models.IntegerField()
    notes = models.TextField(blank=True, null=True)
    #booleanos del modelo
    oil_filter = models.BooleanField(default=False)
    primary_diesel_filter = models.BooleanField(default=False)
    secondary_diesel_filter = models.BooleanField(default=False)
    maintenance_type = models.CharField(max_length=50, choices=MAINTENANCE_TYPE_CHOICES)


class ContractModel (AbstractUUIDPK):
    """Contratos"""
    class Meta:
        db_table='contracts'

    code = models.CharField(max_length=50, verbose_name="codigos de contrato")
    client = models.ForeignKey(ClientModel, verbose_name=("Cliente"), on_delete=models.CASCADE)
    description = models.TextField()

class ExitControlModel(AbstractUUIDPK):
    """Control de Entradas y Salidas"""
    class Meta:
        db_table = 'vehicle_control'
    
    operador = models.ForeignKey(EmployeeModel, verbose_name=(""), on_delete=models.CASCADE)
    vehicle = models.ForeignKey(VehicleModel, verbose_name=(""), on_delete=models.CASCADE)
    contract_case = models.ForeignKey(ContractModel, on_delete=models.CASCADE)
    return_date = models.DateTimeField(null=True)
    kilometers_traveled = models.IntegerField(null=True)
    work_hours = models.TimeField(null=True)


