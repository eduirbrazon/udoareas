from rest_framework.viewsets import GenericViewSet
from rest_framework.mixins import CreateModelMixin, RetrieveModelMixin, ListModelMixin
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from rest_framework.decorators import action
from .models import VehicleModel, SparePartsModel, MaintenanceSheetModel
from .serializers import (VehicleSerializer, VehicleReadAllSerializer, 
                          SparePartSerializer, ShortSparePartSerializer, MaintenanceSerializer, MaintenanceReadSerializer)

class VehiclesView(GenericViewSet, CreateModelMixin, RetrieveModelMixin):
    queryset = VehicleModel.objects.all()
    serializer_class = VehicleSerializer
    
    def create(self, request, *args, **kwargs):
        print(request.data)
        return super().create(request, *args, **kwargs)
    
    def list(self,request):
        vehiclesSerializer = VehicleReadAllSerializer(self.queryset.all(), many=True)
        return Response({'data':vehiclesSerializer.data})

class SparePartsView(GenericViewSet, CreateModelMixin, RetrieveModelMixin, ListModelMixin):
    queryset = SparePartsModel.objects.all()
    serializer_class = SparePartSerializer
    
    def partial_update(self, request, pk):
        object = self.get_object()
        object.quantity += int(request.data['quantity'])
        object.purchase_date = request.data['purchase_date']
        object.save()
        return Response({'message':'Actualizado con exito'})
    
    @action(methods=['get'], detail=False, url_path="short", url_name="short")
    def short(self, request):
        parts_serializers = ShortSparePartSerializer(self.queryset.all(), many=True)
        return Response({'data': parts_serializers.data})

    @action(methods=['get'], detail=False, url_path="filter", url_name="filter")
    def filter(self, request):
        parts_serializers = ShortSparePartSerializer(self.queryset.filter(), many=True)
        return Response({'data': parts_serializers.data})

class MaintenanceView(GenericViewSet, CreateModelMixin, ListModelMixin):
    serializer_class = MaintenanceSerializer
    queryset = MaintenanceSheetModel.objects.all()

    def list(self, request, *args, **kwargs):
        self.serializer_class = MaintenanceReadSerializer
        return super().list(request, *args, **kwargs)