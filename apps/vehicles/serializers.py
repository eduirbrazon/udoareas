import uuid
from datetime import datetime
from django.db import transaction
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from ..persons.serializers import ClientSerializer
from .models import VehicleModel, SparePartsModel, ContractModel, ExitControlModel, MaintenanceSheetModel

class ShortSparePartSerializer(serializers.ModelSerializer):
    class Meta:
        model = SparePartsModel
        fields = ['id','name','model','filter_type']

class SparePartSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(format='hex_verbose', allow_null=True, required=False)

    class Meta:
        model = SparePartsModel
        fields = '__all__'

    def to_internal_value(self, data):
        print(type(data))
        if isinstance(data, str):
            try:
            # Si los datos son un int, trata de obtener el repuesto por su ID.
                return SparePartsModel.objects.get(id=uuid.UUID(data))
            except (ValueError, TypeError, SparePartsModel.DoesNotExist):
                pass
        return super().to_internal_value(data)

    def create(self, validated_data):
        # Intenta obtener el ID desde validated_data.
        print(validated_data)
        id = None
        if isinstance(validated_data, self.Meta.model):
            id = validated_data.id
        if id:
            # Si un UUID es proporcionado, busca el objeto existente.
            print('here')
            try:
                return SparePartsModel.objects.get(id=id)
            except SparePartsModel.DoesNotExist:
                # Si no se encuentra el objeto, puedes decidir lanzar una excepción o manejarlo de otra manera.
                raise serializers.ValidationError("Invalid SparePart UUID provided")
        else:
            # Si no hay 'id', o si el 'id' no es válido, crea un nuevo objeto.
            return super().create(validated_data)


class VehicleSerializer(serializers.ModelSerializer):

    oil_filter = SparePartSerializer(allow_null=True, required=False)
    primary_diesel_filter = SparePartSerializer(allow_null=True, required=False)
    secondary_diesel_filter = SparePartSerializer(allow_null=True, required=False)

    class Meta:
        model = VehicleModel
        fields = '__all__'

    def create(self, validated_data):
        return super().create(validated_data)

    def validate(self, attrs):
        if 'current_fuel' in attrs:
            if attrs['current_fuel'] > attrs['max_fuel_capacity']:
                raise ValidationError('La cantidad de combustible actual no puede ser mayor a la capacidad maxima de combustible')
        if 'current_oil' in attrs:
            if attrs['current_oil'] > attrs['max_oil_capacity']:
                raise ValidationError('La cantidad de aceite actual no puede ser mayor a la capacidad maxima de aceite')
        return super().validate(attrs)

    def create(self, validated_data):
        # Extrae los datos de los repuestos, si están presentes.
        oil_filter_data = validated_data.pop('oil_filter', None)
        primary_diesel_filter_data = validated_data.pop('primary_diesel_filter', None)
        secondary_diesel_filter_data = validated_data.pop('secondary_diesel_filter', None)

        # Procesa y crea/obtiene las instancias de los repuestos si se proporcionaron datos.
        oil_filter_instance = None
        primary_diesel_filter_instance = None
        secondary_diesel_filter_instance = None
        with transaction.atomic():
            if oil_filter_data is not None:
                oil_filter_instance = self.fields['oil_filter'].create(oil_filter_data)
            if primary_diesel_filter_data is not None:
                primary_diesel_filter_instance = self.fields['primary_diesel_filter'].create(primary_diesel_filter_data)
            if secondary_diesel_filter_data is not None:
                secondary_diesel_filter_instance = self.fields['secondary_diesel_filter'].create(secondary_diesel_filter_data)
            # Crea el vehículo con las instancias de los repuestos ya procesadas.
            vehicle = VehicleModel.objects.create(
                **validated_data,
                oil_filter=oil_filter_instance,
                primary_diesel_filter=primary_diesel_filter_instance,
                secondary_diesel_filter=secondary_diesel_filter_instance
            )
        return vehicle

    def update(self, instance, validated_data):
        # Análogamente, maneja la actualización.
        # Este es un esquema básico, necesitarás ajustarlo según tus requisitos específicos.
        for attr, value in validated_data.items():
            if attr in ['oil_filter', 'primary_diesel_filter', 'secondary_diesel_filter']:
                part_instance = getattr(instance, attr)
                if isinstance(value, dict):  # Si se proporciona un dict, actualiza o crea el repuesto.
                    spare_part_serializer = self.fields[attr]
                    if 'id' in value:
                        part_instance = spare_part_serializer.update(part_instance, value)
                    else:
                        part_instance = spare_part_serializer.create(value)
                    setattr(instance, attr, part_instance)
            else:
                setattr(instance, attr, value)
        instance.save()
        return instance

class VehicleReadAllSerializer(serializers.ModelSerializer):
    number = serializers.SerializerMethodField()
    oil_filter = SparePartSerializer(allow_null=True, required=False)
    primary_diesel_filter = SparePartSerializer(allow_null=True, required=False)
    secondary_diesel_filter = SparePartSerializer(allow_null=True, required=False)
    class Meta:
        model = VehicleModel
        fields = '__all__'
    
    def get_number(self, instance):
        if instance:
            digitos_maximos = len(str(self.Meta.model.objects.order_by('number').last().number))
            if digitos_maximos < 5:
                digitos_maximos = 5
            return str(instance.number).zfill(digitos_maximos)

class ContractSerializers(serializers.ModelSerializer):
    
    client = ClientSerializer()
    
    class Meta:
        model = ContractModel
        fields = '__all__'


class ExitControlSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ExitControlModel
        fields = '__all__'

    def update(self, instance, validated_data):
        
        if not 'work_hours' in validated_data and 'return_date' in validated_data:
            validated_data['work_hours'] = (datetime.min + (instance.return_date - instance.created_at)).time()
                
        return super().update(instance, validated_data)

class MaintenanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = MaintenanceSheetModel
        fields = '__all__'
    
    def create(self, validated_data):
        
        if validated_data['oil_filter']:
            oil_filter = validated_data['vehicle'].oil_filter
            oil_filter.quantity -= 1
            oil_filter.save()
        if validated_data['primary_diesel_filter']:
            primary_diesel_filter = validated_data['vehicle'].primary_diesel_filter
            primary_diesel_filter.quantity -= 1
            primary_diesel_filter.save()
        if validated_data['secondary_diesel_filter']:
            secondary_diesel_filter = validated_data['vehicle'].secondary_diesel_filter
            secondary_diesel_filter.quantity -= 1
            secondary_diesel_filter.save()
        return super().create(validated_data)

class MaintenanceReadSerializer(serializers.ModelSerializer):
    vehiculoId = serializers.CharField(source='vehicle.license_plate')
    operadorId = serializers.CharField(source='operator.full_name')
    class Meta:
        model = MaintenanceSheetModel
        fields = '__all__'