from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
import unittest, requests
class TestVehiclesAPI(unittest.TestCase):
    
    def test_vehicles_endpoint(self):
        response = requests.get('http://localhost:8741/api/vehicles')
        self.assertEqual(response.status_code, 200)

    def test_vehicles_post(self):
        response = requests.post('http://localhost:8741/api/vehicles', data={
    "vehicle_type": "normal",
    "number": 1,
    "trademark": "Ford",
    "vehicle_model": "Mustang",
    "license_plate": "ABC123",
    "engine_serial": "1234567890",
    "bodywork_serial": "12345678901234",
    "year": 2020,
    "color": "Rojo",
    "insurance_policy": "ABC12345678901",
    "insurance_policy_expiration_date": "2022-01-01",
    "status": "disponible",
    "current_location": "Madrid",
    "current_fuel": 50,
    "max_fuel_capacity": 70,
    "current_oil": 5,
    "max_oil_capacity": 10,
    "mileage": 12345,
    "kms_to_maintenance": 10000,
    "worked_hours": 1200,
    "oil_filter": {
        "name": "Filtro de aceite",
        "model": "Filtro de aceite",
        "item_code": "FiltroAceite",
        "quantity": 1
    },
    "primary_diesel_filter": {
        "name": "Filtro de Gasoil primario",
        "model": "Filtro de Gasoil primario",
        "item_code": "FiltroGasoilPrimario",
        "quantity": 1
    },
    "secondary_diesel_filter": {
        "name": "Filtro de Gasoil secundario",
        "model": "Filtro de Gasoil secundario",
        "item_code": "FiltroGasoilSecundario",
        "quantity": 1
    }
})  
        print(response.json())
        self.assertEqual(response.status_code, 201)
    
    def test_spare_parts_endpoint(self):
        response = requests.get('http://localhost:8741/api/vehicles/spareparts')
        self.assertEqual(response.status_code, 200)
    
    def test_spare_parts_post(self):
        response = requests.post('http://localhost:8741/api/vehicles/spareparts', data={
    "name": "Filtro de aceite",
    "model": "Filtro de aceite",
    "item_code": "FiltroAceite",
    "quantity": 1,
    "filter_type": "N/A",
    "supplier": "N/A",
    "min_stock_level": 0,
    "purchase_date": "2021-01-01"
})
        self.assertEqual(response.status_code, 201)
    
    def test_maintenance_endpoint(self):
        response = requests.get('http://localhost:8741/api/vehicles/maintenance')
        self.assertEqual(response.status_code, 200)
        

def test_create_spare_part(self):
        """
        Asegura que podemos crear un nuevo objeto SparePartsModel.
        """
        url = reverse('spareparts-list')  # Asume que tienes una URL nombrada 'spareparts-list' para tu endpoint
        data = {
            'name': 'Filtro de Aceite',
            'model': 'FA-2020',
            'item_code': 'FA20203421',
            'quantity': 10,
            'filter_type': 'Oil',  # Asume que 'Oil' es un valor válido en FILTER_TYPES
            'supplier': 'Proveedor Principal',
            'min_stock_level': 5,
            'purchase_date': '2021-07-21',
        }
        response = self.client.post(url, data, format='json')