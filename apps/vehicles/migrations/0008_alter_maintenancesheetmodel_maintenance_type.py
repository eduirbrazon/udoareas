# Generated by Django 5.0.2 on 2024-03-06 15:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0007_vehiclemodel_max_oil_capacity_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='maintenancesheetmodel',
            name='maintenance_type',
            field=models.CharField(choices=[('Preventivo', 'Preventivo'), ('Correctivo', 'Correctivo'), ('Inspeccion', 'Inspección'), ('Rotacion_neumaticos', 'Rotación de Neumáticos'), ('Cambio_aceite', 'Cambio de Aceite')], max_length=50),
        ),
    ]
