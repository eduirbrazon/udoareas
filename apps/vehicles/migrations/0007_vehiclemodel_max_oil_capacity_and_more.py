# Generated by Django 5.0.2 on 2024-03-06 05:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0006_alter_vehiclemodel_worked_hours'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiclemodel',
            name='max_oil_capacity',
            field=models.IntegerField(default=0, help_text='Capacidad máxima de Aceite en litros'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='vehiclemodel',
            name='max_fuel_capacity',
            field=models.IntegerField(help_text='Capacidad máxima de combustible en litros'),
        ),
    ]
