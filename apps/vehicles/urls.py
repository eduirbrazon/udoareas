from django.urls import path
from rest_framework.routers import DefaultRouter
from .views import VehiclesView, SparePartsView, MaintenanceView

router = DefaultRouter(trailing_slash=False)
router.register(r'/sparts/?', SparePartsView)
router.register(r'/maintenance/?', MaintenanceView)
router.register(r'/?', VehiclesView)

urlpatterns = [
    
]
urlpatterns+=router.urls
