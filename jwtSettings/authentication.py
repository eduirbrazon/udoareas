from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.exceptions import AuthenticationFailed


class CJWTAuthentication(JWTAuthentication):

    def authenticate(self, request):
        response = super().authenticate(request)
        if response is not None:
            requestToken = self.get_raw_token(
                self.get_header(request)).decode()
            user_token = response[0].session_token
            if requestToken == user_token:
                return response
            raise AuthenticationFailed('Authenticacion no Valida')
        return response