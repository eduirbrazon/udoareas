import jwt
from decouple import config
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.views import TokenObtainPairView, TokenViewBase
from apps.users.models import UserModel
from .serializers import  CustomTokenObtainPairSerializer, TokenVerifySerializer

from rest_framework_simplejwt.views import TokenObtainPairView

class GetAccesToken(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        return Response({'message': 'Logueo Exitoso! Bienvenido', 'access': response.data['access']})

class TokenVerifyView(APIView):

    def get(self, request, *args, **kwargs):
        data = {
            'token': request.headers['Authorization'].split(' ')[1]
        }
        token_serializer_verify = TokenVerifySerializer(data=data)
        try:
            token_serializer_verify.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])

        return Response(token_serializer_verify.validated_data, status=status.HTTP_200_OK)
    

class LoggoutView(APIView):

    def get(self, request):
        print('here')
        print(request.headers)
        jwT = request.META.get('HTTP_AUTHORIZATION').split(' ')[1]
        decoded_token = jwt.decode(jwT, config(
            'SECRET_KEY'), algorithms=['HS256'])
        user = UserModel.objects.filter(id=decoded_token['user_id']).first()
        if not user:
            return Response({'status': 'ERROR', 'message': 'Usuario no encontrado'}, status=status.HTTP_404_NOT_FOUND)
        if user.session_token != jwT:
            return Response({'status': 'ERROR', 'message': 'Token invalido'}, status=status.HTTP_401_UNAUTHORIZED)
        user.session_token = None
        user.save(update_fields=['session_token'])
        return Response({'message': 'Sesion Cerrada Satisfactoriamente'}, status=status.HTTP_200_OK)
