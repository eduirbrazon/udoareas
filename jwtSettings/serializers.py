import jwt
from decouple import config
from datetime import datetime
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework_simplejwt.tokens import UntypedToken
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.settings import api_settings

class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        'no_active_account': 'Usuario o Contraseña incorrectos.',
        'no_matching_credentials': 'Usuario o Contraseña incorrectos.',
    }
    @classmethod
    def get_token(self, user):
        token = super().get_token(user)
        token['rol'] = user.rol()
        return token

    def validate(self, attrs):
        data = super().validate(attrs)
        print(self.user)
        if self.user.session_token:
            try:
                decoded_token = jwt.decode(self.user.session_token, config(
                    'SECRET_KEY'), algorithms=['HS256'])
                dt_object = datetime.fromtimestamp(
                    decoded_token['exp'])
                if dt_object > datetime.now():
                    self.user.session_token = None
                    self.user.save(update_fields=['session_token'])
                    raise ValidationError(
                        'Habia una Session Activa y fue Cerrada, Intente Nuevamente')
                else:
                    self.user.session_token = None
                    self.user.save(update_fields=['session_token'])
            except jwt.ExpiredSignatureError:
                pass
        refresh = self.get_token(self.user)
        # data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        self.user.session_token = data['access']
        self.user.save(update_fields=['session_token'])
        return data
    

class TokenVerifySerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):
        token = UntypedToken(attrs['token'])
        return {}